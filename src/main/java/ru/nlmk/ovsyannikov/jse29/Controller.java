package ru.nlmk.ovsyannikov.jse29;

import java.util.Scanner;

public class Controller {
    protected final Scanner scanner = new Scanner(System.in);

    public void displayWelcome() {
        System.out.println("** WELCOME**");
        System.out.println("Use commands - sum, factorial, fibonacci, exit");
        System.out.println("****");
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public int exit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int calcSum(){
        System.out.println("[CALCULATE SUM]");
        System.out.println("PLEASE, ENTER FIRST NUMBER:");
        final String n1 = scanner.nextLine();
        System.out.println("[PLEASE, ENTER SECOND NUMBER:]");
        final String n2 = scanner.nextLine();
        long result = Service.sum(n1, n2);
        System.out.println("SUM = " + result);
        return 0;
    }

    public int calcFactorial(){
        System.out.println("[CALCULATE FACTORIAL]");
        System.out.println("PLEASE, ENTER NUMBER:");
        final String n1 = scanner.nextLine();
        System.out.println("FACTORIAL = " + Service.factorial(n1));
        return 0;
    }

    public int calcFibonacci(){
        System.out.println("[CALCULATE FIBONACCI]");
        System.out.println("PLEASE, ENTER NUMBER:");
        final String n1 = scanner.nextLine();
        String result = Service.fibonacci(n1).toString();
        System.out.println("FIBONACCI SEQUENCE = " + result);
        return 0;
    }

}
