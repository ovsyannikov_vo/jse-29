package ru.nlmk.ovsyannikov.jse29;

public class TerminalConst {
    public static final String EXIT = "exit";
    public static final String SUM = "sum";
    public static final String FACTORIAL = "factorial";
    public static final String FIBONACCI = "fibonacci";
}
