package ru.nlmk.ovsyannikov.jse29;

import java.util.Scanner;

import static ru.nlmk.ovsyannikov.jse29.TerminalConst.*;

public class Main {
    private final Controller controller = new Controller();

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);

        final Main main = new Main();
        main.run(args);
        main.controller.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            main.run(command);
        }
    }

    public void run(final String[] args) {
        if (args.length == 0) return;
        if (args.length <1) return;
        final String param=args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;

        switch (param){
            case EXIT: return controller.exit();
            case SUM: return controller.calcSum();
            case FACTORIAL: return controller.calcFactorial();
            case FIBONACCI: return controller.calcFibonacci();

            default: return controller.displayError();
        }
    }
}
