package ru.nlmk.ovsyannikov.jse29;

import java.util.ArrayList;
import java.util.List;

public class Service {

    private static boolean isLong(String s) throws NumberFormatException {
        try {
            Long.parseLong(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static long sum(String arg1, String arg2) {
        if (!isLong(arg1) || !isLong(arg2)) {
            throw new IllegalArgumentException();
        }
        return Long.parseLong(arg1) + Long.parseLong(arg2);
    }

    public static long factorial(String arg) {
        if (!isLong(arg) || Long.parseLong(arg) <= 0) {
            throw new IllegalArgumentException();
        }
        int result = 1;
        for (int i = 1; i <= Long.parseLong(arg); i++) {
            result = Math.multiplyExact(result, i);
        }
        return result;
    }

    public static List<Long> fibonacci(String arg) {
//    public static Long[] fibonacci(String arg) {
        if (!isLong(arg) || Long.parseLong(arg) <= 0) {
            throw new IllegalArgumentException();
        }
        long x = Long.parseLong(arg);
        long n0 = 1;
        long n1 = 1;
        long n2;
        List<Long> chislaFibonacci = new ArrayList<>();
        chislaFibonacci.add(n0);
        chislaFibonacci.add(n1);
        if (x == 1) return chislaFibonacci;
        int stop = 0;
        while (stop == 0) {
            n2 = n0 + n1;
            if (n2 > x) {
                throw new IllegalArgumentException();
            }
            chislaFibonacci.add(n2);
            if (n2 == x) {
                stop = 1;
            }
            n0 = n1;
            n1 = n2;
        }
        /*int index = chislaFibonacci.size();
        return chislaFibonacci.toArray(new Long[index]);*/
        return chislaFibonacci;
    }

}

