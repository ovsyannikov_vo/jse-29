package ru.nlmk.ovsyannikov.jse29;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ServiceTest {

    @Test
    void sumCorrect() {
        assertEquals(4, Service.sum( "1", "3"));
    }

    @Test
    void sumCorrect1() {
        assertEquals(2, Service.sum( "-1", "3"));
        assertEquals(-2, Service.sum( "-1", "-1"));
    }

    @Test
    void sumException() {
        assertThrows(IllegalArgumentException.class, () -> Service.sum("1.2", "3"));
    }

    @Test
    void sumException2() {
        assertThrows(IllegalArgumentException.class, () -> Service.sum("a", "3"));
    }

    @Test
    void factorialCorrect() {
        assertEquals(24, Service.factorial( "4"));
    }

    @Test
    void factorialException() {
        assertThrows(IllegalArgumentException.class, () -> Service.factorial("a"));
        assertThrows(IllegalArgumentException.class, () -> Service.factorial("-1"));
    }

    @Test
    void factorialException2() {
        assertThrows(ArithmeticException.class, () -> Service.factorial("2256477558314496007"));
    }

    @Test
    void fibonacciCorrect() {
        List<Long> list = Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L);
        assertEquals(list, Service.fibonacci( "13"));
    }

    @Test
    void fibonacciException() {
        assertThrows(IllegalArgumentException.class, () -> Service.fibonacci("1.2"));
    }

    @Test
    void fibonacciException2() {
        assertThrows(IllegalArgumentException.class, () -> Service.fibonacci("-3"));
    }

    @Test
    void fibonacciException3() {
        assertThrows(IllegalArgumentException.class, () -> Service.fibonacci("4"));
    }

}